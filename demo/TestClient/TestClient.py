#
#    This script is a part of the Tango2Epics project.
#    The script tests the response time for reading and writing to 
#    scalar and spectrum attributes of the Tango2Epics device.
#    Testing is performed on attributes with different data types.
#    Client is not configurable and works with provided demo configuration.
#    Results are displayed via console.
#


if __name__ == "__main__":
    import sys
    import PyTango
    from PyTango import *
    from datetime import datetime

    scalar_att_names = ["att_double_one", "att_float_one", "att_int_one", "att_short_one", "att_byte_one"]
    array_att_names = ["att_doubleA_one", "att_floatA_one", "att_intA_one", "att_shortA_one", "att_byteA_one"]

    ##SCALAR
    try:
        print("Measuring response time for scalar attributes:")
        tangotest = DeviceProxy("Epics/Test/AllScalarACS")
        for att_name in scalar_att_names:
            valueR = 0
            valueW = 0
            for index in range(0,999):
                a = datetime.now()
                tangotest.write_attribute(att_name,1)
                b = datetime.now()
                c = b - a
                valueW = valueW + c.microseconds
    
                a = datetime.now()
                tangotest.read_attribute(att_name)
                b = datetime.now()
                c = b - a
                valueR = valueR + c.microseconds
            print("\nDevice: Epics/Test/AllScalarASC")
            print("Attribute: " + att_name)        
            print("Average response time: ")
            print "Read: " , valueR/1000, " microseconds"
            print "Write: " , valueW/1000, " microseconds"
    except:
        print("Error occurred whilst accessing the device!")

    ##ARRAY
    try:
        print("\nMeasuring response time for array attributes:")
        tangotest = DeviceProxy("Epics/Test/AllArrayA")
        for att_name in array_att_names:
            valueR = 0
            valueW = 0
            for index in range(0,999):
                a = datetime.now()
                tangotest.write_attribute(att_name,[1,1,1,1,1,1,1,1,1,1])
                b = datetime.now()
                c = b - a
                valueW = valueW + c.microseconds
    
                a = datetime.now()
                tangotest.read_attribute(att_name)
                b = datetime.now()
                c = b - a
                valueR = valueR + c.microseconds
            print("\nDevice: Epics/Test/AllArrayA")
            print("Attribute: " + att_name)      
            print("Average response time: ")
            print "Read: " , valueR/1000, " microseconds"
            print "Write: " , valueW/1000, " microseconds"
    except:
        print("Error occurred whilst accessing the device!")

