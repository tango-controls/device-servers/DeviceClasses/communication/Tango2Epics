//=============================================================================
//
// file :        PvCommand.h
//
// description : Tango Command implementations that trigger epics operations.
//
// project : Tango2Epics
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Author: Vid Juvan $
//
//=============================================================================

#ifndef PVCOMMAND_H_
#define PVCOMMAND_H_


class PvBasicCommand{

public:
	virtual void execute() {};
	virtual ~PvBasicCommand() {};
};


template <class T>
class PvCommand: public PvBasicCommand{

public:

	std::string name;
	long data_type;
	int data_type_epics;
	T data[1] = {0};
	EpicsThreadHandler* threadHandler;

	PvCommand(const char* _name,long _data_type, int _data_type_epics, EpicsThreadHandler* _threadHandler, T _data, double accessTimeout){

		threadHandler = _threadHandler;
		threadHandler->registerNew(_name,_data_type_epics, accessTimeout);

		name = _name;
		data[0] = _data;
		data_type = _data_type;
		data_type_epics = _data_type_epics;
	};
	~PvCommand() {};

	virtual void execute(){
		//std::cout << "CMD THREAD HANLDER START" << std::endl;		//DEBUG only
    string error;
		threadHandler->runAndWait(name.c_str(),1,(void *)data,error);
    if(!error.empty())
        Tango::Except::throw_exception("PVCmdError",
                                       error,
                                       "PvCommand::execute()",
                                       Tango::WARN);
		//std::cout << "CMD THREAD HANLDER END" << std::endl;		//DEBUG only
	}

};


class PvStringCommand: public PvBasicCommand{

public:

	std::string name;
	long data_type;
	int data_type_epics;
	char* data = new char[256];
	EpicsThreadHandler* threadHandler;

	PvStringCommand(const char* _name,long _data_type, int _data_type_epics, EpicsThreadHandler* _threadHandler, const char* _data, double accessTimeout){

		threadHandler = _threadHandler;
		threadHandler->registerNew(_name,_data_type_epics, accessTimeout);

		name = _name;
		strcpy(data, _data);
		data_type = _data_type;
		data_type_epics = _data_type_epics;
	};
	~PvStringCommand() {
		delete[] data;
	};

	virtual void execute(){
		//std::cout << "CMD THREAD HANLDER START" << std::endl;		//DEBUG only
    string error;
		threadHandler->runAndWait(name.c_str(),1,(void *)data,error);
    if(!error.empty())
      Tango::Except::throw_exception("PVCmdError",
                                     error,
                                     "PvStringCommand::execute()",
                                     Tango::WARN);
		//std::cout << "CMD THREAD HANLDER END" << std::endl;		//DEBUG only
	}

};



#endif /* PVCOMMAND_H_ */
