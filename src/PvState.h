//=============================================================================
//
// file :        PvState.h
//
// description : Tango State implementations, controlled by epics.
//
// project : Tango2Epics
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Author: Vid Juvan $
//
//=============================================================================

#ifndef PVSTATE_H_
#define PVSTATE_H_

#include <tango.h>
#include <pthread.h>


class PvBasicState{

public:
	virtual ~PvBasicState() {};
};


template <class T>
class PvState: public PvBasicState{

public:

	typedef struct{
		T value;
		Tango::DevState state;
	}stateMapping;

	typedef struct{
		Tango::DeviceImpl *dev;
		std::vector<stateMapping> mappings;
		pthread_mutex_t *mutex;
	}changeStateParam;

	std::string name;
	long data_type;
	int data_type_epics;
	T data;
	EpicsThreadHandler* threadHandler;

	changeStateParam param;

	PvState(const char* _name,long _data_type, int _data_type_epics, EpicsThreadHandler* _threadHandler, std::vector<stateMapping> _stateMappings, Tango::DeviceImpl *dev, double subscriptionCycle, pthread_mutex_t* _stateMutex){

		threadHandler = _threadHandler;
		param.dev = dev;
		param.mappings = _stateMappings;
		param.mutex = _stateMutex;

		threadHandler->subscribeNew(_name,_data_type_epics, subscriptionCycle, changeState, (void*)&param);

		name = _name;
		data_type = _data_type;
		data_type_epics = _data_type_epics;
	};
	~PvState() {};

	static void changeState(struct event_handler_args param){

		T realValue = *((T*)(param.dbr));
		changeStateParam* parameters = ((changeStateParam*)(param.usr));

		for(unsigned int i = 0; i < parameters->mappings.size(); i++){
			if(parameters->mappings[i].value == realValue){
				//std::cout << "FOUND MATCH FOR FUNCTION: " << parameters->mappings[i].state << std::endl;		//DEBUG only
				pthread_mutex_lock(parameters->mutex);
				parameters->dev->set_state(parameters->mappings[i].state);
				pthread_mutex_unlock(parameters->mutex);
			}
		}

	}

};



class PvStringState: public PvBasicState{

public:

	typedef struct{
		char value[256];
		Tango::DevState state;
	}stateMapping;

	typedef struct{
		Tango::DeviceImpl *dev;
		std::vector<stateMapping> mappings;
		pthread_mutex_t *mutex;
	}changeStateParam;

	std::string name;
	long data_type;
	int data_type_epics;
	const char* data;
	EpicsThreadHandler* threadHandler;

	changeStateParam param;

	PvStringState(const char* _name,long _data_type, int _data_type_epics, EpicsThreadHandler* _threadHandler, std::vector<stateMapping> _stateMappings, Tango::DeviceImpl *dev, double subscriptionCycle, pthread_mutex_t* _stateMutex){

		threadHandler = _threadHandler;
		param.dev = dev;
		param.mappings = _stateMappings;
		param.mutex = _stateMutex;

		threadHandler->subscribeNew(_name,_data_type_epics, subscriptionCycle, changeState, (void*)&param);

		name = _name;
		data_type = _data_type;
		data_type_epics = _data_type_epics;
	};
	~PvStringState() {};

	static void changeState(struct event_handler_args param){

		char* realValue = ((char*)(param.dbr));
		changeStateParam* parameters = ((changeStateParam*)(param.usr));

		for(unsigned int i = 0; i < parameters->mappings.size(); i++){
			if(strcmp(realValue, parameters->mappings[i].value) == 0){
				//std::cout << "FOUND MATCH FOR FUNCTION: " << parameters->mappings[i].state << std::endl;		//Debug only
				pthread_mutex_lock(parameters->mutex);
				parameters->dev->set_state(parameters->mappings[i].state);
				pthread_mutex_unlock(parameters->mutex);
			}
		}

	}

};


#endif /* PVSTATE_H_ */
